# Atividade Git Saber

## Documentação usando Markdown
 A documentação nos projetos é muito importante, e para isso o gitlab
 disponibiliza o uso de markdown. Vamos testar o quanto o markdown é
 poderoso, usando o guia do gitlab: https://gitlab.com/help/user/markdown.

### #1 - Básico
Escrever uma frase por linha(paragrafo).

**Escrever em negrito**
  
_Escrever em itálico_
  
~~Escrever texto riscado~~
  
**_Escrever em negrito e italico ao mesmo tempo_**

### #2 - Links
Link normal: www.gitlab.com

[Link com texto customizado](https://www.gitlab.com)

###### Imagem:

![Imagem](https://gitlab.com/help/user/img/markdown_logo.png)

### #3 - Blocos de citação
> Bloco de texto de citação

> Outro bloco de texto

###### Blocos de texto de citação com realce de sintaxe:
```javascript
function test() {
  console.log("Código em javascript");
}
```
```python
def function():
    print "Código em python"
```
```
Nenhuma linguagem indicada
```

### #4 - Listas
1. Primeiro item da lista numerada
2. Segundo item
   1. Primeiro item da sub-lista
   2. Segundo item da sub-lista
3. Terceiro

* Item da lista não numerada
* Outro item

###### Lista de tarefas
- [x] Tarefa completa
- [ ] Tarefa incompleta
    - [ ] Sub-tarefa 1
    - [x] Sub-tarefa 2

### #5 - Tabela

| Alinhado para esquerda | Centralizado | Alinhado para direita |
| :--------------------- | :----------: | --------------------: |
| Célula 1               | Célula 2     | Célula 3              |
| Célula 4               | Célula 5     | Célula 6              |
